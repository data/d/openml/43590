# OpenML dataset: AI-Simulated-Games-of-Machi-Koro

https://www.openml.org/d/43590

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This data is the result of using neural networks and reinforcement learning to simulate the board game "Machi Koro". 
Here is the source code for the AI and simulation: https://github.com/mcandocia/machi_ai
Content
Each row is a single turn in a game, corresponding to the state at the beginning of the turn, and there are 10,000 games total. There are 4 players, so all columns except for gameid and turnid reference a specific player. Each player takes turns performing actions, which can involve gaining coins, losing coins to other players, building properties, stealing coins from other players, and possibly switching properties with another player. The results of each turn largely depend on a single or double dice-roll.
The goal of the game is to build four specific properties, which are represented by the variables pXstation, pXshoppingmall, pXamusementpark, pXradio_tower, where X represents the player who owns it.
Variables:
gameid - ID of the game being played
turnid - Turn number of the game. An additional turn is added at the end of each game to show the final state.
The following attributes have a prefix pX_, where X is the ID of the player, which also corresponds to the turn order (0=first, 1=second, etc.):
win - Has the player won the game (class)
coins - The number of coins the player has (integer)

(anything else) - Each of these corresponds to the number of properties a player has constructed by this point in the game.

Inspiration
I was playing the game the other night, and I wondered if I could teach an AI the game, given its relatively simple rules. It is a fairly straightforward game, and I wanted to record the game history so that I could extract insights, such as strategy and overall usefulness of certain properties/cards.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43590) of an [OpenML dataset](https://www.openml.org/d/43590). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43590/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43590/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43590/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

